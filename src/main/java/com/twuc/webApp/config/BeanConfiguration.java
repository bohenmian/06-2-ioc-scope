package com.twuc.webApp.config;

import com.twuc.webApp.model.LazyBean;
import com.twuc.webApp.model.MyLogger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class BeanConfiguration {

    private MyLogger myLogger;

    public BeanConfiguration(MyLogger myLogger) {
        this.myLogger = myLogger;
    }

    @Bean
    @Lazy
    public LazyBean getLazyBean() {
        return new LazyBean(myLogger);
    }
}

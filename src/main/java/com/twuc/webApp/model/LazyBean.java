package com.twuc.webApp.model;


public class LazyBean {

    private Integer id;
    private MyLogger logger;

    public LazyBean() {
    }

    public LazyBean(MyLogger logger) {
        logger.markLogger("this is singleton lazy init construction");
        this.logger = logger;
    }

    public Integer getId() {
        logger.markLogger("this is singleton lazy init method");
        return id;
    }
}

package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

@Component
public class MyListener {

    private Integer id;
    private MyLogger logger;

    public MyListener(MyLogger logger) {
        logger.markLogger("this is construction method");
        this.logger = logger;
    }

    public Integer getId() {
        logger.markLogger("this is getId method");
        return id;
    }
}

package com.twuc.webApp.model;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyLogger {

    private List<String> lines = new ArrayList<>();

    public void markLogger(String log) {
        this.lines.add(log);
    }

    public List<String> getLines() {
        return lines;
    }
}

package com.twuc.webApp.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {

    private Logger logger;
    public Integer id;

    public PrototypeDependentWithProxy() {
    }

    public PrototypeDependentWithProxy(Integer id) {
        this.id = id;
    }

    public PrototypeDependentWithProxy(Logger logger) {
        logger.markLogger("this is proxy construction");
        this.logger = logger;
    }

    public void getString() {
        return;
    }

    public Integer getId() {
        return id;
    }
}

package com.twuc.webApp.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {

    private SingletonDependent singletonDependent;

    @Autowired
    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent) {
        this.singletonDependent = singletonDependent;
    }

    public SingletonDependent getSingletonDependent() {
        return singletonDependent;
    }
}

package com.twuc.webApp.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimplePrototypeScopeClass {

    private Integer id;
    private MyLogger logger;

    public SimplePrototypeScopeClass(MyLogger logger) {
        logger.markLogger("this is prototype construction method");
        this.logger = logger;
    }

    public Integer getId() {
        logger.markLogger("this is prototype getId method");
        return id;
    }
}

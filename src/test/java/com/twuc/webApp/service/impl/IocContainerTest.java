package com.twuc.webApp.service.impl;

import com.twuc.webApp.model.*;
import com.twuc.webApp.service.InterfaceOne;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.ClassUtils;

import javax.validation.Valid;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;

import static org.junit.jupiter.api.Assertions.*;

public class IocContainerTest {


    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_return_the_same_object_implements() {
        InterfaceOne bean = context.getBean(InterfaceOne.class);
        InterfaceOneImpl contextBean = context.getBean(InterfaceOneImpl.class);
        assertEquals(bean, contextBean);
        assertSame(bean, contextBean);
    }

    @Test
    void should_return_the_same_object_with_extends() {
        Son son = context.getBean(Son.class);
        Father father = context.getBean(Father.class);
        assertEquals(son, father);
        assertSame(son, father);
    }

    @Test
    void should_return_the_same_object_with_abstract_extends() {
        AbstractBaseClass baseClass = context.getBean(AbstractBaseClass.class);
        DerivedClass bean = context.getBean(DerivedClass.class);
        assertEquals(baseClass, bean);
        assertSame(baseClass, bean);
    }

    @Test
    void should_return_two_object_with_prototype() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass contextBean = context.getBean(SimplePrototypeScopeClass.class);
        assertNotEquals(bean, contextBean);
        assertNotSame(bean, contextBean);
    }

    @Test
    void should_return_two_create_time_with_singleton() {
        MyListener bean = context.getBean(MyListener.class);
        MyLogger contextBean = context.getBean(MyLogger.class);
        assertEquals("this is construction method", contextBean.getLines().get(0));
        bean.getId();
        assertEquals("this is getId method", contextBean.getLines().get(1));
    }

    @Test
    void should_return_two_create_time_with_prototype() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        MyLogger contextBean = context.getBean(MyLogger.class);
        assertEquals("this is prototype construction method", contextBean.getLines().get(1));
        bean.getId();
        assertEquals("this is prototype getId method", contextBean.getLines().get(2));
        // single bean的lazy-init都是false，因此在ApplicationContext启动时就会实例化，而prototype则会在第一次使用时实例化
    }

    @Test
    void should_lazy_load_when_when_config_lazy_annotation() {
        LazyBean bean = context.getBean(LazyBean.class);
        MyLogger contextBean = context.getBean(MyLogger.class);
        bean.getId();
        assertEquals("this is singleton lazy init construction", contextBean.getLines().get(1));
        assertEquals("this is singleton lazy init method", contextBean.getLines().get(2));
    }

    @Test
    void should_return_same_singleton_bean_and_not_same_prototype_bean() {
        PrototypeScopeDependsOnSingleton bean = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton bean1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertNotSame(bean, bean1);
        assertSame(bean.getSingletonDependent(), bean1.getSingletonDependent());
    }

    @Test
    void should_return_same_singleton_bean_when_depends_on_prototype() {
        SingletonDependsOnPrototype bean = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype bean1 = context.getBean(SingletonDependsOnPrototype.class);
        assertSame(bean, bean1);
        assertSame(bean.getPrototypeDependent(), bean1.getPrototypeDependent());
    }

    @Test
    void should_return_same_singleton_and_prototype_bean_when_use_proxy_mode() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getPrototypeDependentWithProxy().getString();
        bean.getPrototypeDependentWithProxy().getString();
        Logger bean2 = context.getBean(Logger.class);
        assertEquals(2, bean2.getLines().size());
    }

    @Test
    void should_create_one_singleton_and_different_prototype_method_call_given_singleton_depends_on_prototype_when_prototype_call_to_string() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        PrototypeDependentWithProxy prototypeDependentWithProxy = bean.getPrototypeDependentWithProxy();
        prototypeDependentWithProxy.toString();
        prototypeDependentWithProxy.toString();
        Logger bean2 = context.getBean(Logger.class);
        assertEquals(2, bean2.getLines().size());
    }

    @Test
    void should_return_the_proxy_instance() throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        Class<? extends PrototypeDependentWithProxy> prototypeDependentWithProxy = bean.getPrototypeDependentWithProxy().getClass();
        assertTrue(ClassUtils.isCglibProxyClass(prototypeDependentWithProxy));
    }
}
